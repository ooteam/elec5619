package au.usyd.elec5619.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.orm.hibernate.annotation.HibernateSessionFactory;

import au.usyd.elec5619.domain.ExerciseCommitment;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.Progress;
import au.usyd.elec5619.domain.SmokingProgress;

@HibernateSessionFactory("database.properities")
public class ProgressDaoTest extends UnitilsJUnit4 {
	ExerciseProgress exerciseProgress;
	SmokingProgress smokingProgress;
	
	@HibernateSessionFactory
	private SessionFactory sessionFactory;
	
	public void setUp() {
		this.exerciseProgress = new ExerciseProgress();
		this.smokingProgress= new SmokingProgress();
		
	}
	
	@Test
	public void testGetProgressByCommitment() {
		ProgressDao dao = new ProgressDaoImpl();
		ExerciseCommitment commitment = new ExerciseCommitment();
		sessionFactory.getCurrentSession().save(commitment);
		
		List<Progress> progresses = dao.getProgressesByCommitment(commitment);
		
		System.out.println(progresses.get(0));
	}

}
