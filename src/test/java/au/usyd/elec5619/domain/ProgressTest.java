package au.usyd.elec5619.domain;

import java.util.Date;

import junit.framework.TestCase;

public class ProgressTest extends TestCase {
	private ExerciseProgress loseWeightProgress;
	private SmokingProgress quitSmokingProgress;
	
	public void setUp() {
		this.loseWeightProgress = new ExerciseProgress();
		this.quitSmokingProgress= new SmokingProgress();
	}
	
	public void testSetAndGetCommitment() {
		Commitment commitment = new ExerciseCommitment();
		
		assertNull(loseWeightProgress.getCommitment());
		assertNull(quitSmokingProgress.getCommitment());
		
		loseWeightProgress.setCommitment(commitment);
		quitSmokingProgress.setCommitment(commitment);
		
		assertEquals(loseWeightProgress.getCommitment(), commitment);
		assertEquals(quitSmokingProgress.getCommitment(), commitment);
	}
	
	public void testSetAndGetProgressDate() {
		Date date = new Date(2015, 9 ,20);
		
		assertNull(loseWeightProgress.getProgressDate());
		assertNull(quitSmokingProgress.getProgressDate());
		
		loseWeightProgress.setProgressDate(date);
		quitSmokingProgress.setProgressDate(date);
		
		assertEquals(loseWeightProgress.getProgressDate(), date);
		assertEquals(quitSmokingProgress.getProgressDate(), date);
	}
	
	public void testSetAndGetComment() {
		String comment = "Test comment";
		
		assertNull(loseWeightProgress.getComment());
		assertNull(quitSmokingProgress.getComment());
		
		loseWeightProgress.setComment(comment);
		quitSmokingProgress.setComment(comment);
		
		assertEquals(loseWeightProgress.getComment(), comment);
		assertEquals(quitSmokingProgress.getComment(), comment);
	}
	
	public void testSetAndGetCurrentWeight() {
		assertEquals(loseWeightProgress.getCurrentWeight(), 0.0);
		loseWeightProgress.setCurrentWeight(300.0);
		assertEquals(loseWeightProgress.getCurrentWeight(), 300.0);
	}
	
	public void testSetAndGetExerciseType() {
		assertNull(loseWeightProgress.getExerciseType());
		loseWeightProgress.setExerciseType(ExerciseType.JOGGING);
		assertEquals(loseWeightProgress.getExerciseType(), ExerciseType.JOGGING);
	}
	
	public void testSetAndGetExerciseDone() {
		assertEquals(loseWeightProgress.getExerciseDone(), 0.0);
		loseWeightProgress.setExerciseDone(200.0);
		assertEquals(loseWeightProgress.getExerciseDone(), 200.0);
	}
	
	public void testSetAndGetNumberOfCigarette() {
		assertEquals(quitSmokingProgress.getNumberOfCigarette(), 0);
		quitSmokingProgress.setNumberOfCigarette(3);
		//assertEquals(quitSmokingProgress.getNumberOfCigarette(), new Integer(3));
	}
}
