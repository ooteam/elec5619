$(document).ready(function() {
	$(".progress-bar").css("width", function() { return $(this).attr("aria-valuenow") + "%"; });
	
	$(".progress").tipsy({gravity: 's'});
	
	$(".commitment-row").mouseover(function() {
		var position = $(this).position();
		var icon = $("<i style='; top: 15px; background-color: #f5f5f5' class='fa fa-pencil'></i>");
		icon.css({
			position: "absolute",
			top: position.top + 15,
			left: position.left + 860,
			color: "#777"
		});
		$(this).append(icon);
	});
	$(".commitment-row").mouseout(function() {
		$(this).children("i").remove();
	});
	$(".commitment-row").click(function() {
		window.document.location = $(this).data("href");
	})
});