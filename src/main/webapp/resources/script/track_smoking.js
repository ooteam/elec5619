$(document).ready(function() {
	var margin = { top: 20, right: 200, bottom: 50, left: 100 },
    	width = 680 - margin.left - margin.right,
    	grid_size = Math.floor(width / 10),
    	gap = 7;
    	legend_element_width = grid_size * 2
    	buckets = 7;
	
	var colors = ["#c6dbef","#9ecae1","#6baed6","#4292c6","#2171b5","#08519c","#08306b"],
		days = ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"];
	
	var format = d3.time.format("%Y-%m-%d");
	
	function date_equal(a, b) {
		return a.getDate() == b.getDate() && 
			a.getMonth() == b.getMonth();
	}
	
	var id = d3.select("#visualisation input").property("value");
	d3.json("/elec5619/api/progress/" + id, function(err, data) {
		if (err) {
			console.error(err);
			return;
		}
		if (data.length == 0) {
			d3.select("#visualisation")
				.append("p")
				.style("padding", "80px 218px")
				.html("No progress has been recorded");
			return;
		}
		
		var data = _.unique(data, function(d) { return new Date(d.progressDate).getDate() + new Date(d.progressDate).getMonth()});
		
		var commitment = data[0].commitment;
		
		data = data.sort(function(a, b) { return d3.ascending(new Date(a.progressDate), new Date(b.progressDate))});
		var weeks = Math.ceil((commitment.endDate - commitment.startDate) / 1000 / 60 / 60 / 24 / 7);
		var height = weeks * 50;
		
		var average = d3.sum(data, function(d) { return d.numberOfCigarette; }) / data.length;
		
		var streak_start = new Date(data[0].progressDate);
		var streak = 0;
		var max_streak = 0;
		var max_streak_start = new Date(data[0].progressDate),
			max_streak_end = null;
		for (var i = 0; i < data.length; i++) {
			var a = new Date(data[i].progressDate);
			var b = new Date(streak_start.getTime() + streak * 60000 * 60 * 24);
			if (date_equal(a, b) && data[i].numberOfCigarette == 0) {
				streak += 1;
			} else {
				if (streak > max_streak) {
					max_streak = streak - 1;
					max_streak_start = streak_start;
					max_streak_end = new Date(max_streak_start.getTime() + (max_streak) * 60000 * 60 *24);
				}
				streak = 1;
				streak_start = new Date(data[i].progressDate);
			}
			
		}
		if (streak > max_streak) {
			max_streak = streak - 1;
			max_streak_start = streak_start;
			max_streak_end = new Date(max_streak_start.getTime() + (max_streak) * 60000 * 60 *24);
		}
		
		d3.select(".avg .value").html(average.toFixed(2))
		d3.select(".avg .date").html(format(new Date(data[0].progressDate)) + " - " + format(new Date(data[data.length-1].progressDate)))
		
		if (max_streak == 0) {
			d3.select(".streak .value").html("0");
			d3.select(".streak .date").html("Not applicable")
		} else {
			d3.select(".streak .value").html(max_streak + (max_streak > 1 ? " days" : " day"));
			d3.select(".streak .date").html(format(max_streak_start) + " - " + format(max_streak_end));
		}
		
		var start = new Date(data[0].progressDate);
		start = new Date(start.getTime() - (start.getDay() - 1) * 60000 * 60 * 24);
		
		var _data = data.slice();
		var ptr = 0;
		var goal_index = 0;
		for (var i = 0; i < 7 * weeks; i++) {
			var b = new Date(start.getTime() + i * 60000 * 60 * 24);
			if (ptr < _data.length) {
				var a = new Date(_data[ptr].progressDate);
				if (date_equal(a, b)) {
					ptr += 1;
					continue;
				} 
			}
			if (date_equal(b, new Date(commitment.endDate))) {
				goal_index = i;
				data.splice(i, 0, {progressDate: b, numberOfCigarette: -1, goal: true, currentWeight: 0});
			} else {
				data.splice(i, 0, {progressDate: b, numberOfCigarette: -1, goal: false, currentWeight: 0});
			}
		}
		
		data.forEach(function(d, i) {
			d.progressDate = new Date(d.progressDate);
			d.index = i;
		});
		
		var svg = d3.select("#visualisation").append("svg")
			.attr("width", width + margin.left + margin.right)	
			.attr("height", height + margin.top + margin.bottom)
		  .append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		var week_labels = svg.selectAll(".week")
			.data(d3.range(weeks))
			.enter()
		  .append("text")
			.text(function(d) { return d + 1; })
			.attr("x", 0)
			.attr("y", function(d, i) { return i * (grid_size + gap); })
			.style("text-anchor", "end")
			.attr("transform", "translate(-6," + grid_size / 1.5 + ")")
			.attr("class", "week");
		
		var day_labels = svg.selectAll(".day")
			.data(days)
			.enter()
		  .append("text")
		    .text(function(d) { return d; })
		    .attr("x", function(d, i) { return i * (grid_size + gap); })
		    .attr("y", 0)
		    .style("text-anchor", "middle")
		    .attr("transform", "translate(" + grid_size / 2 + ", -6)")
		    .attr("class", "day");

		var color_scale = d3.scale.quantile()
			.domain([0, buckets - 1, d3.max(data, function(d) { return d.numberOfCigarette; })])
			.range(colors);
		
		var records = svg.selectAll(".record")
			.data(data, function(d) { return d.progressDate; });
		
		records.enter()
		  .append("rect")
		    .attr("x", function(d) { return (d.index % 7) * (grid_size + gap); })
		    .attr("y", function(d) { return Math.floor(d.index / 7) * (grid_size + gap); })
		    .attr("rx", 4)
		    .attr("ry", 4)
		    .attr("title", function(d) { return format(d.progressDate) + "<br/> cigar: " + d.numberOfCigarette; })
		    .attr("class", function(d) { return "record bordered" + (d.goal ? " goal" : ""); })
		    .attr("width", grid_size)
		    .attr("height", grid_size)
		    
		records.transition().duration(1000)
			.style("fill", function(d) { return d.numberOfCigarette == -1 ? "#eee" : color_scale(d.numberOfCigarette); });
		
		svg.append("text")
			.attr("x", grid_size / 2 + (goal_index % 7) * (grid_size + gap) - 5.5)
			.attr("y", grid_size / 2 + (Math.floor(goal_index / 7)) * (grid_size + gap))
			.attr("dy", ".35em")
			.text("G");
		
		$('.record').tipsy({gravity: 'se', html: true});
		
		var legend = svg.selectAll(".legend")
        	.data([0].concat(color_scale.quantiles()), function(d) { return d; });
		
		legend.enter().append("g")
			.attr("class", "legend");
		
		legend.append("rect")
			.attr("x", function(d, i) { return legend_element_width * i - 95; })
			.attr("y", height - 10)
			.attr("width", legend_element_width)
			.attr("height", grid_size / 2)
			.style("fill", function(d, i) { return colors[i]; });
		
		 legend.append("text")
		 	.attr("class", "mono")
         	.text(function(d) { return "> " + Math.round(d); })
         	.attr("x", function(d, i) { return legend_element_width * i - 95; })
         	.attr("y", height + grid_size - 10);
		 
	});
	
});