<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ page session="false"%>
<html>
<head>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<link href="<c:url value="/resources/css/track.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/hello.css" />" rel="stylesheet"type="text/css" />
<script src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<title>Home</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/usernavbar.jsp"%>
	<div class="track">
		<div class="container">
			<div class="col-sm-12">
				<h2>Tracking commitment</h2>
				<c:forEach var="commitment" items="${commitments}">
					<div class="row">
						<div class="col-sm-12">
							<a href="<c:url value="/track/show/${commitment.getId()}"/>">
								<p>
									<c:out value="${commitment.getCommitmentTypeName()}"></c:out>
								</p>
							</a>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	</div>
</body>
</html>