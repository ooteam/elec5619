<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<script
	src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<link href="<c:url value="/resources/css/formdesign.css" />" rel="stylesheet"
	type="text/css" />
<title>Login</title>
</head>
<body>
	<div class="formdesign">
		<nav class="navbar">
			<div class="container nav-container">
				<div class="navbar-header">
					<a class="navbar-brand" href="<c:url value="/" />">Carrot and Stick</a>
				</div>
				<ul class="nav nav-pills pull-right">
					<li role="presentation"><a href="<c:url value="/" />">Home</a></li>
					<li role="presentation"><a href="<c:url value="register" />">Register</a></li>
					<li role="presentation"><a href="<c:url value="login"/>">Login</a></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<div class="col-md-6 col-md-push-3">
				<h1>Login</h1>
				<br />
				<c:url value="/login" var="loginUrl" />
				<form:form action="${loginUrl}" method="post"
					modelAttribute="loginForm">
					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<c:if test="${param.logout != null}">
						<div class="alert alert-success">You have been logged out.</div>
					</c:if>
					<div class="form-group">
						<label>Email address</label>
						<form:input type="email" path="j_email" class="form-control"
							value="" />
					</div>
					<div class="form-group">
						<label>Password</label>
						<form:input type="password" path="j_password" class="form-control"
							value="" />
					</div>
					<button type="submit" class="btn btn-default">Login</button>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>