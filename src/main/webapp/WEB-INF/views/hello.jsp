<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<script
	src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/script/jquery.tipsy.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/script/hello.js" />" type="text/javascript"></script>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/hello.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/tipsy.css" />" rel="stylesheet"
	type="text/css" />
</head>
<body>
		<%@ include file="/WEB-INF/views/usernavbar.jsp"%>
		
		<div class="content-container">
		<h1>Hello, ${userEmail}!</h1>
		<br />
		<h3>Your commitments</h3>
		<br />
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Type</th>
					<th>Weekly Goal</th>
					<th>Weekly Bet</th>
					<th>Total Amount Donated</th>
					<th>Progress</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${commitments}" var="commitment">
					<tr class="commitment-row" data-href=<c:url value="/track/show/${commitment.getId()}"/>>
						<td>
							<c:if test="${commitment.type == 'Exercise'}">
								<c:out value="${commitment.type}"/>
							</c:if>
							<c:if test="${commitment.type == 'Weight'}">
								<c:out value="Weight loss"/>
							</c:if>
							<c:if test="${commitment.type == 'Smoke'}">
								<c:out value="Quit smoking"/>
							</c:if>
						</td>
						<td>
							<c:if test="${commitment.type == 'Exercise'}">
								<c:out value="${commitment.weeklyGoal} exercises"/>
							</c:if>
							<c:if test="${commitment.type == 'Weight'}">
								<c:out value="${commitment.weeklyLossGoal}%"/>
							</c:if>
							<c:if test="${commitment.type == 'Smoke'}">
								<c:out value="${commitment.weeklyGoal} cigarettes max"/>
							</c:if>
						</td>
						<td><c:out value="${commitment.weeklyBet}" /></td>
						<td><c:out value="${commitment.totalFail * commitment.weeklyBet }"/></td>
						<td>
							<div style="width: 230px" class="progress" title="Ends in <fmt:formatDate type="date" value="${commitment.endDate}"/>">
								<c:set var="span" value="${commitment.endDate.getTime() - commitment.startDate.getTime()}"/>
								<c:set var="diff" value="${commitment.endDate.getTime() - today}"/>
								<c:set var="days_span" value="${span / 1000 / 60 / 60 / 24}"/>
								<c:set var="days_left" value="${diff / 1000 / 60 / 60 / 24}"/>
								
								<div class="progress-bar" role="progressbar" aria-valuenow="${100 - diff / span * 100}"
									aria-valuemin="0" aria-valuemax="100">
									<fmt:formatNumber type="number" maxFractionDigits="0" value="${days_left}" /> days left</div>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<h3>People You Follow</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Email</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${following}" var="user">
					<tr>
						<td>
							<c:out value="${user.email}"/>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>

</body>
</html>