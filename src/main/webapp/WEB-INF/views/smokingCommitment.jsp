<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<script
	src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link href="<c:url value="/resources/css/newcommitment.css" />"
	rel="stylesheet" type="text/css" />
<title>New Quitting Smoking Commitment</title>
</head>
<body>
	<div class="newcommitment">
		<%@ include file="/WEB-INF/views/usernavbar.jsp"%>
		<div class="container">
			<div class="col-md-6 col-md-push-3">
				<h1>New Quitting Smoking Commitment</h1>
				<br />
				<c:url value="/postCommitment" var="commitmentUrl" />
				<form:form action="${commitmentUrl}" method="post"
					modelAttribute="commitmentForm" enctype="utf8">
					<div class="form-group">
						<label>No. of Max Cigarettes per Week</label>
						<form:input path="weeklyGoal" class="form-control" value="" />
					</div>
					<div class="form-group">
						<label>Weekly Bet</label>
						<form:input path="weeklyBet" class="form-control" value="" />
					</div>
					<div class="form-group">
						<label>End Date</label>
						<form:input type="text" path="endDate" class="form-control"
							 />
					</div>
					<form:input type="hidden" path="type" class="form-control" value="Smoke"/>

					<button type="submit" class="btn btn-default">Submit</button>
				</form:form>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function() {
   		 $('dropdown-toggle').dropdown()
	});
</script>
	
</body>
</html>