<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<script
	src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />

<link href="<c:url value="/resources/css/hello.css" />" rel="stylesheet"
	type="text/css" />
</head>
<body>
		<%@ include file="/WEB-INF/views/usernavbar.jsp"%>
		
		<div class="content-container">
		<h1>People Who Follow You</h1>
		<br />
		<br />
		<table class="table">
			<thead>
				<tr>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${followers}" var="user">
					<tr>
						<td>
							<c:out value="${user.email}"/>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>

<script>
	$(document).ready(function() {
   		 $('dropdown-toggle').dropdown()
	});
</script>

</body>
</html>