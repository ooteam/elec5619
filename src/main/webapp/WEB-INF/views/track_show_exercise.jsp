<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ page session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8" />
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<link href="<c:url value="/resources/css/track.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/tipsy.css" />" rel="stylesheet"
	type="text/css" />
<link href="<c:url value="/resources/css/hello.css" />" rel="stylesheet"
	type="text/css" />

<script src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="<c:url value="/resources/script/jquery.tipsy.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/script/d3.min.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/script/lodash.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/script/validator.js" />"
	type="text/javascript"></script>
<script src="<c:url value="/resources/script/track_exercise.js" />"
	type="text/javascript"></script>
<title>Home</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/usernavbar.jsp"%>
	<div class="track">
		<div class="container">
			<div class="col-sm-12">
				<h2>Your commitment</h2>
				<ul>
					<li>Type: ${commitmentType}</li>
					<li>Charity: ${commitment.getCharity()}</li>
					<li>Weekly bet: $${commitment.getWeeklyBet()}</li>
				</ul>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Progress</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-8" id="visualisation">
								<input type="hidden" value="${commitmentId}" />
							</div>
						</div>
						<div class="row summary-stats">
							<div class="col-sm-4 avg">
								<p>Average exercise done</p>
								<p class="value"></p>
								<p class="date"></p>
							</div>
							<div class="col-sm-4 streak">
								<p>Longest streak</p>
								<p class="value"></p>
								<p class="date"></p>
							</div>
							<div class="col-sm-4 max">
								<p>Highest exercise done</p>
								<p class="value"></p>
								<p class="date"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="line panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Weight chart</h3>
					</div>
					<div class="panel-body">
						<div id="line-visualisation"></div>
					</div>
				</div>
				<h2>Report your progress</h2>
				<c:choose>
					<c:when test="${!reported}">
						<form:form action="/elec5619/track/create-exercise/"
							commandName="progress" method="POST" enctype="utf8">
							<form:errors path="*" cssClass="errorblock" element="div"></form:errors>
							<div class="form-group">
								<label>Exercise Type</label> 
								<form:select path="exerciseType" class="form-control">
									<c:forEach var="type" items="${exerciseTypeList}">
										<form:option value="${type.value}">${type.key}</form:option>
									</c:forEach>
								</form:select>
							</div>
							<div class="form-group">
								<label>Exercise Done (km)</label>
								<form:input type="number" path="exerciseDone" class="form-control"
									value="" required="true" />
							</div>
							<div class="form-group">
								<label>Current Weight (kg)</label>
								<form:input type="number" path="currentWeight"
									class="form-control" value="" required="true" />
							</div>
							<div class="form-group">
								<label>Comment</label>
								<form:textarea path="comment" class="form-control" value="" />
							</div>
							<form:input type="hidden" path="commitment" class="form-control"
								value="${commitmentId}" />
							<button type="submit" class="btn btn-default">Submit</button>
						</form:form>
					</c:when>
					<c:otherwise>
						You have already reported today
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	</div>
</body>
</html>