<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ page session="false"%>
<html>
<head>
<link
	href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.0/css/bootstrap.css"
	rel="stylesheet" />
<link href="<c:url value="/resources/css/home.css" />" rel="stylesheet"
	type="text/css" />
<script
	src="${pageContext.request.contextPath}/webjars/jquery/1.11.1/jquery.min.js"></script>
<title>Home</title>
</head>
<body>
	<div class="home">
	<nav class="navbar">
		<div class="container nav-container">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">Carrot and Stick</a>
			</div>
			<ul class="nav nav-pills pull-right">
			<li role="presentation"><a href="<c:url value="" />">Home</a></li>
				<li role="presentation"><a href="<c:url value="register" />">Register</a></li>
				<li role="presentation"><a href="<c:url value="login"/>">Login</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<div class="main">
			<h2 class="title">Make a commitment</h2>
			<div class="commitment">
				<c:url value="/register" var="registerUrl" />
				<form class="form-inline" action="${registerUrl} " method="get">
					I committed to <select class="form-control"><option>Lose weight</option></select>
					or I pay <input style="width: 60px"class="form-control" type="text" value="30" /> to charity
					<input class="btn btn-default btn-lg" type="submit" value="Make a commitment"/>
				</form>
			</div>
		</div>
		
	</div>
	</div>
</body>
</html>