<nav class="navbar">
	<div class="container nav-container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<c:url value="/" />">Carrot and
				Stick</a>
		</div>
		<ul class="nav nav-pills pull-right">
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" role="button" aria-haspopup="true"
				aria-expanded="false">Make a Commitment <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="<c:url value="/exerciseCommitment"/>">Exercise</a></li>
					<li><a href="<c:url value="/weightCommitment"/>">Weight
							Loss</a></li>
					<li><a href="<c:url value="/smokingCommitment"/>">Quit
							Smoking</a></li>
				</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" role="button" aria-haspopup="true"
				aria-expanded="false">Following <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="<c:url value="/activities"/>">See Activities</a></li>
					<li><a href="<c:url value="/followers"/>">See Followers</a></li>
					<li><a href="<c:url value="/listUsers"/>">Find Users</a></li>
				</ul></li>


			<li role="presentation"><a
				href="<c:url value="/j_spring_security_logout" />">Logout</a></li>
		</ul>
	</div>
</nav>