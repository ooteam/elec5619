package au.usyd.elec5619.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.web.HomeController;

@Transactional
@Repository(value = "userDao")
public class UserDao {
	private SessionFactory sessionFactory;
	
	private static final Logger logger = LoggerFactory.getLogger(UserDao.class);
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public Account findUserByEmail(String email) {
		List<Account> users = new ArrayList<Account>();
		
		users = sessionFactory.getCurrentSession()
				.createQuery("from Account where email=?")
				.setParameter(0, email)
				.list();
		
		logger.info("There are " + users.size() + " with email " + email);
		
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Account findUserById(Long id) {
		List<Account> users = new ArrayList<Account>();
		
		users = sessionFactory.getCurrentSession()
				.createQuery("from Account where id=?")
				.setParameter(0, id)
				.list();
		
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public boolean validateUserCredentials(String email, String password) {
		List<Account> users = new ArrayList<Account>();
		
		users = sessionFactory.getCurrentSession()
				.createQuery("from Account where email=? and password=?")
				.setParameter(0, email)
				.setParameter(1, password)
				.list();
		
		if (users.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public void create(Account user) {
		sessionFactory.getCurrentSession().save(user);
	}
	
	public List<Account> getAllAccounts() {
		List<Account> users = new ArrayList<Account>();
				
				users = sessionFactory.getCurrentSession()
							  .createQuery("from Account")
							  .list();
		
		return users;
	}
	
	public void followUser(Account follower, Account following) {
		logger.info("Adding user follow");
		logger.info(follower.getEmail());
		logger.info(following.getEmail());
		follower = (Account) sessionFactory.getCurrentSession().get(Account.class, follower.getId());
		following = (Account) sessionFactory.getCurrentSession().get(Account.class, following.getId());
		follower.getFollowing().add(following);
	}

}
