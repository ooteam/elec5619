package au.usyd.elec5619.dao;

import java.util.ArrayList;
import java.util.List;

import javax.management.Query;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.BankAccount;

@Transactional
@Repository(value = "bankAccountDao")
public class BankAccountDao {
	private SessionFactory sessionFactory;

	private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void createBankAccount(BankAccount bankAccount) {
		sessionFactory.getCurrentSession().save(bankAccount);
	}

	public BankAccount getBankAccountByUserId(Long userId) {
		List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

		bankAccounts = sessionFactory.getCurrentSession()
					   .createQuery("from BankAccount where userId=?").setParameter(0, userId)
					   .list();
		
		return bankAccounts.get(0);
	}
	

}