package au.usyd.elec5619.dao;

import java.util.List;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.Progress;

public interface ProgressDao {
	public void report(Progress progress);
	public List<Progress> getProgressesByUser(Account user);
	public List<Progress> getProgressesByCommitment(Commitment commitment);
}
