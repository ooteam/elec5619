package au.usyd.elec5619.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.Progress;

@Transactional
@Repository(value = "progressDao")
public class ProgressDaoImpl implements ProgressDao {
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void report(Progress progress) {
		sessionFactory.getCurrentSession().save(progress);
	}

	@Override
	public List<Progress> getProgressesByCommitment(Commitment commitment) {
		List<Progress> progresses = sessionFactory.getCurrentSession()
				.createQuery("from Progress where commitmentid=? order by progressDate asc")
				.setParameter(0,  commitment.getId())
				.list();
		
		return progresses;
	}

	@Override
	public List<Progress> getProgressesByUser(Account user) {
		List<Progress> progresses = sessionFactory.getCurrentSession()
				.createQuery("from Progress where userId=? order by progressDate asc")
				.setParameter(0,  user.getId())
				.list();
		
		return progresses;
	}
	

}
