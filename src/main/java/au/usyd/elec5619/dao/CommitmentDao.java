package au.usyd.elec5619.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseCommitment;

@Transactional
@Repository(value="commitmentDao")
public class CommitmentDao {

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Commitment getCommitmentById(Long id){
		List<Commitment> commitmentList = 
				sessionFactory.getCurrentSession()
				.createQuery("from Commitment where commitmentId=?")
				.setParameter(0,  id)
				.list();
		if (commitmentList.size() > 0) return commitmentList.get(0);
		else return null;
	}
	
	public List<Commitment> getCommitmentsByUserId(Long userId){
		List<Commitment> commitmentList = 
				sessionFactory.getCurrentSession()
				.createQuery("from Commitment where userId=?")
				.setParameter(0,  userId)
				.list();
		return commitmentList;
	}
	
	public void createCommitment(Commitment commitment){
		sessionFactory.getCurrentSession().save(commitment);
	}
	
	public List<Commitment> getAllActiveCommitments(){
		Date currentDate = new Date();
		List<Commitment> commitmentList = 
				sessionFactory.getCurrentSession()
				.createQuery("from Commitment where endDate > ?")
				.setDate(0,  currentDate)
				.list();
		return commitmentList;
	}
	
	public List<Commitment> getActiveCommitmentsByUser(Long userId) {
		Date currentDate = new Date();
		List<Commitment> commitmentList = 
				sessionFactory.getCurrentSession()
				.createQuery("from Commitment where userId=? and endDate > :today")
				.setParameter(0,  userId)
				.setDate("today", currentDate)
				.list();
		return commitmentList;
	}
}
