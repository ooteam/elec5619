package au.usyd.elec5619.service;

import java.util.List;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.exception.EmailExistsException;

public interface IUserService {
	public void register(Account user) throws EmailExistsException;
	public boolean validateCredentials(String email, String password);
	public Account getUserByEmail(String email);
	public List<Account> getAllAccounts();
	public Account getUserById(Long id);
	public Boolean followUser(Account follower, Account following);
}