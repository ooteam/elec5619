package au.usyd.elec5619.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.BankAccountDao;
import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.BankAccount;

@Service(value = "bankService")
@Transactional
public class BankService implements IBankService {

	private static final Logger logger = LoggerFactory.getLogger(BankService.class);

	@Autowired
	private BankAccountDao bankAccountDao;

	@Override
	public void createBankAccountForUser(Account user) {
		BankAccount bankAccount = new BankAccount(user);
		bankAccountDao.createBankAccount(bankAccount);
		logger.info("Finish creating bank account for user");
	}

	@Override
	public Double deductMoneyFromUser(Account user, Double amount) {
		logger.info("Deducting $" + amount + " from " + user.getEmail());
		BankAccount bank = bankAccountDao.getBankAccountByUserId(user.getId());
		bank.setAmount(bank.getAmount()-amount);
		return bank.getAmount();
	}

}
