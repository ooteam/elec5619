package au.usyd.elec5619.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.UserDao;
import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.exception.EmailExistsException;

@Service(value = "userService")
public class UserService implements IUserService {
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private BankService bankService;
	
	@Override
	public void register(Account user) throws EmailExistsException {
		String email = user.getEmail();
		
		if (userDao.findUserByEmail(email) != null) {
			throw new EmailExistsException();
		}
		
		userDao.create(user);
		bankService.createBankAccountForUser(user);
	}

	@Override
	public boolean validateCredentials(String email, String password) {
		return userDao.validateUserCredentials(email, password);
	}

	@Override
	public Account getUserByEmail(String email) {
		return userDao.findUserByEmail(email);
	}
	
	@Override
	public List<Account> getAllAccounts(){
		return userDao.getAllAccounts();
	}
	
	public Account getUserById(Long id) {
		return userDao.findUserById(id);
	}
	
	public Boolean followUser(Account follower, Account following) {
		Boolean success = false;
		if(!follower.getFollowing().contains(following) && !follower.equals(following)) {
			userDao.followUser(follower, following);
			success = true;
		}
		return success;
	}
}
