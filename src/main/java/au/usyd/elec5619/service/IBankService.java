package au.usyd.elec5619.service;

import au.usyd.elec5619.domain.Account;

public interface IBankService {

	public void createBankAccountForUser(Account user);
	public Double deductMoneyFromUser(Account user, Double amount);
}
