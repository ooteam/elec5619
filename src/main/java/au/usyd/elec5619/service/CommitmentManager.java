package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.CommitmentDao;
import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseCommitment;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.Progress;
import au.usyd.elec5619.domain.SmokingProgress;
import au.usyd.elec5619.domain.WeightCommitment;
import au.usyd.elec5619.domain.WeightProgress;
import au.usyd.elec5619.domain.SmokingCommitment;
import au.usyd.elec5619.web.CommitmentController;

@Service(value="commitmentManager")
@Transactional
public class CommitmentManager implements ICommitmentManager {
	
	private static final Logger logger = LoggerFactory.getLogger(CommitmentManager.class);
	
	@Autowired
	private CommitmentDao commitmentDao;
	
	@Autowired
	private BankService bankService;
	
	@Autowired 
	private ProgressService progressService;

	@Override
	public Commitment getCommitmentById(Long commitmentId) {
		
		return commitmentDao.getCommitmentById(commitmentId);
		
	}

	@Override
	public List<Commitment> getCommitmentsByUser(Account user) {
		
		return commitmentDao.getCommitmentsByUserId(user.getId());
		
	}

	@Override
	public List<Commitment> getActiveCommitmentsByUser(Account user) {
		List<Commitment> activeCommitments = new ArrayList<Commitment>();
		activeCommitments = commitmentDao.getActiveCommitmentsByUser(user.getId());
		return activeCommitments;
	}

	@Override
	public void penalizeUser(Commitment commitment) {
		logger.info("Penalizing " + commitment.getUserId() + "amount is " + commitment.getWeeklyBet());
		commitment.setTotalFail(commitment.getTotalFail()+1);
		bankService.deductMoneyFromUser(commitment.getUserId(), commitment.getWeeklyBet().doubleValue());
	}

	@Override
	public void createCommitment(Commitment commitment) {
		//TODO: Check commitment class and call the method from DAO
		logger.info("adding commitment via DAO");
		commitmentDao.createCommitment(commitment);
		
	}
	
	@Scheduled(cron="0 0 8 * * *")
	public void dailyPenaltyCheck(){
		
		//Get active commitments that were created on the same day of week as today
		Date currentDate = new Date();
		Calendar now = Calendar.getInstance();
		Calendar commitmentCal = Calendar.getInstance();
		now.setTime(currentDate);
		int dowNow = now.get(Calendar.DAY_OF_WEEK);
		List<Commitment> activeCommitments = new ArrayList<Commitment>();
		for (Commitment c : commitmentDao.getAllActiveCommitments()) {
			commitmentCal.setTime(c.getStartDate());
			if(dowNow == commitmentCal.get(Calendar.DAY_OF_WEEK)) {
				activeCommitments.add(c);
			}
		}
		
		now.add(Calendar.DATE, -7);
		
		//Check last progress via progress service
		for (Commitment c : activeCommitments) {
			List<Progress> progList = progressService.getProgressesByCommitment(c);
			int listSize = progList.size();
			if (listSize == 0){
				//Penalize if no progress at all
				penalizeUser(c);
				continue;
			}
			
			//Penalize if no progress in the past week
			if (progList.get(listSize-1).getProgressDate().before(now.getTime())) {
				penalizeUser(c);
			} else {
				//Compare progress value with goal
				if (c.getType().equals("Exercise")) {
					//Check progress from the past week
					int counter = listSize-1;
					int totalExercise = 0;
					while (progList.get(counter).getProgressDate().after(now.getTime())) {
						totalExercise += 1; //Simply count how many times they exercise
						counter--;
						if(counter < 0) break;
					}
					if (totalExercise < ((ExerciseCommitment)c).getWeeklyGoal()) {
						penalizeUser(c);
					}
				} else if (c.getType().equals("Weight")) {
					WeightCommitment weightCom = (WeightCommitment) c;
					double goalWeight = weightCom.getLatestWeight()*(1-(weightCom.getWeeklyLossGoal()/100));
					if (((WeightProgress)progList.get(listSize-1)).getCurrentWeight() > goalWeight) {
						penalizeUser(c);
					}
				} else if (c.getType().equals("Smoke")) {
					int counter = listSize-1;
					int totalCigarettes = 0;
					while (progList.get(counter).getProgressDate().after(now.getTime())) {
						totalCigarettes += ((SmokingProgress)progList.get(counter)).getNumberOfCigarette();
						counter--;
						if(counter < 0) break;
					}
					if (totalCigarettes > ((SmokingCommitment)c).getWeeklyGoal()) {
						penalizeUser(c);
					}
				}
			}
		}
		
	}

}
