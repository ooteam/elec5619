package au.usyd.elec5619.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.usyd.elec5619.dao.ProgressDao;
import au.usyd.elec5619.dao.ProgressDaoImpl;
import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.Progress;

@Service(value = "progressService")
@Transactional
public class ProgressService implements IProgressService {
	@Autowired
	ProgressDao progressDao = new ProgressDaoImpl();
	
	@Autowired
	ICommitmentManager commitmentService = new CommitmentManager();
	
	@Override
	public void report(Progress progress) {
		progressDao.report(progress);
	}

	@Override
	public List<Progress> getProgressesByCommitment(Commitment commitment) {
		return progressDao.getProgressesByCommitment(commitment);
	}

	@Override
	public List<Progress> getProgressesByUser(Account user) {
		List<Commitment> commitments = commitmentService.getCommitmentsByUser(user);
		List<Progress> progresses = new ArrayList<Progress>();
		
		for (Commitment commitment : commitments) {
			progresses.addAll(progressDao.getProgressesByCommitment(commitment));
		}
		
		return progresses;
	}

	@Override
	public List<Progress> getProgressById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Progress getLastProgressByCommitment(Commitment commitment) {
		List<Progress> progresses = progressDao.getProgressesByCommitment(commitment);
		if (progresses.isEmpty()) {
			return null;
		}
		return progresses.get(progresses.size() - 1);
	}

	@Override
	public Progress getLastProgressByUser(Account user) {
		List<Progress> progresses = progressDao.getProgressesByUser(user);
		if (progresses.isEmpty()) {
			return null;
		}
		return progresses.get(progresses.size() - 1);
	}

	@Override
	public void remove(Progress progress) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HashMap<Account, Progress> getFriendsProgressesByUser(Account user) {
		// TODO Auto-generated method stub
		return null;
	}

}
