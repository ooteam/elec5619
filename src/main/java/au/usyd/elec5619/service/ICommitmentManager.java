package au.usyd.elec5619.service;

import java.util.List;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseCommitment;


public interface ICommitmentManager {

	public Commitment getCommitmentById(Long commitmentId);
	public List<Commitment> getCommitmentsByUser(Account user);
	public List<Commitment> getActiveCommitmentsByUser(Account user);
	public void penalizeUser(Commitment commitment);
	public void createCommitment(Commitment commitment);
	public void dailyPenaltyCheck();
	
}
