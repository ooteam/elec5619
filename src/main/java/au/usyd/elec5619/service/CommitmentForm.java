package au.usyd.elec5619.service;

import java.util.Date;

public class CommitmentForm {
	
	private Long userId;
	private Date endDate;
	private Integer weeklyBet;
	private Integer weeklyGoal;
	private Double weeklyLossGoal;
	private Double startWeight;
	private String type; //Weight, Exercise, Smoke
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getWeeklyBet() {
		return weeklyBet;
	}
	public void setWeeklyBet(Integer weeklyBet) {
		this.weeklyBet = weeklyBet;
	}
	public Integer getWeeklyGoal() {
		return weeklyGoal;
	}
	public void setWeeklyGoal(Integer weeklyGoal) {
		this.weeklyGoal = weeklyGoal;
	}
	public Double getWeeklyLossGoal() {
		return weeklyLossGoal;
	}
	public void setWeeklyLossGoal(Double weeklyLossGoal) {
		this.weeklyLossGoal = weeklyLossGoal;
	}
	public Double getStartWeight() {
		return startWeight;
	}
	public void setStartWeight(Double startWeight) {
		this.startWeight = startWeight;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
