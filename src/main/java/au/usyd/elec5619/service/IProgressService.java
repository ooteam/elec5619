package au.usyd.elec5619.service;

import java.util.HashMap;
import java.util.List;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.Progress;

public interface IProgressService {
	public void report(Progress progress);
	public void remove(Progress progress);
	public List<Progress> getProgressesByCommitment(Commitment commitment);
	public Progress getLastProgressByCommitment(Commitment commitment);
	public List<Progress> getProgressesByUser(Account user);
	public HashMap<Account, Progress> getFriendsProgressesByUser(Account user);
	public Progress getLastProgressByUser(Account user);
	public List<Progress> getProgressById(Long id);
}
