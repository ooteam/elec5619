package au.usyd.elec5619.service;

import org.hibernate.validator.constraints.NotEmpty;

public class LoginForm {
	
	@NotEmpty(message="Email is required")
	private String j_email;
	
	@NotEmpty(message="Password is required")
	private String j_password;

	public String getJ_email() {
		return j_email;
	}

	public void setJ_email(String j_email) {
		this.j_email = j_email;
	}

	public String getJ_password() {
		return j_password;
	}

	public void setJ_password(String j_password) {
		this.j_password = j_password;
	}
	
	

}
