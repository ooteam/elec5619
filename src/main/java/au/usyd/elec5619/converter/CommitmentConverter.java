package au.usyd.elec5619.converter;

import java.text.ParseException;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.service.ICommitmentManager;

@Component
public class CommitmentConverter implements Converter<String, Commitment> {
	private static final Logger logger = LoggerFactory.getLogger(CommitmentConverter.class);
	@Autowired
    private ICommitmentManager commitmentService;
	@Override
	public Commitment convert(String id) {
		return commitmentService.getCommitmentById(Long.parseLong(id));
	}

}
