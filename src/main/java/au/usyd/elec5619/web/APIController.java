package au.usyd.elec5619.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.Progress;
import au.usyd.elec5619.service.CommitmentManager;
import au.usyd.elec5619.service.ProgressService;
import au.usyd.elec5619.service.UserService;

@Controller
@RequestMapping("/api/")
public class APIController {
	private static final Logger logger = LoggerFactory.getLogger(TrackCommitmentController.class);
	
	@Autowired
	CommitmentManager commitmentService = new CommitmentManager();
	
	@Autowired
	ProgressService progressService = new ProgressService();

	@Autowired
	UserService userService = new UserService();
	
	@RequestMapping(value = "/progress/{id}", method = RequestMethod.GET)
	public @ResponseBody List<Progress> getProgress(@PathVariable String id) {
		Commitment commitment = commitmentService.getCommitmentById(Long.parseLong(id));
		List<Progress> progresses = progressService.getProgressesByCommitment(commitment);
		
		return progresses;
	}
	
	@RequestMapping(value = "/progress/last", method = RequestMethod.GET)
	public @ResponseBody Progress getLastProgress() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		
		Progress progress = progressService.getLastProgressByUser(user);
		
		return progress;
	}
	
	@RequestMapping(value = "/commitment/{id}", method = RequestMethod.GET)
	public @ResponseBody Commitment getCommitment(@PathVariable String id) {
		Commitment commitment = commitmentService.getCommitmentById(Long.parseLong(id));
		
		return commitment;
	}
	
	@RequestMapping(value = "/progress/commitment/{id}", method = RequestMethod.GET)
	public @ResponseBody Progress getLastProgressCommitment(@PathVariable String id) {
		Commitment commitment = commitmentService.getCommitmentById(Long.parseLong(id));
		Progress progress = progressService.getLastProgressByCommitment(commitment);
 		
		return progress;
	}
}