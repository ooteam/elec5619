package au.usyd.elec5619.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.usyd.elec5619.service.IUserService;
import au.usyd.elec5619.service.LoginForm;

@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private AuthenticationSuccessHandler successHandler;
	
	@Autowired
	private IUserService userService;

	@RequestMapping(value ="/login")
	 public String login(LoginForm loginForm) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {

		    /* The user is logged in :) */
		    return "redirect:/hello";
		}
        return "login";
	}
	
	@RequestMapping(value="/login", method = RequestMethod.POST)
	public String customLogin(HttpServletRequest request, HttpServletResponse response,
			@Valid LoginForm loginForm, BindingResult result) throws IOException, ServletException {
		
		logger.info("Posting to /login");
		
		String email = loginForm.getJ_email();
		String password = loginForm.getJ_password();
		//TODO: validate from database
		
        if(!userService.validateCredentials(email, password)) {
            result.reject("login.fail","Bad username/password");
        }
		
		if(result.hasErrors()) {
			logger.info("Result has error", result.getAllErrors());
			return "login";
		}
		
		Authentication authentication = 
				new UsernamePasswordAuthenticationToken(email, password,
						AuthorityUtils.createAuthorityList("ROLE_USER"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		successHandler.onAuthenticationSuccess(request, response, authentication);
		return null;
		
	}
	
}
