package au.usyd.elec5619.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.dao.UserDao;
import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.service.CommitmentManager;
import au.usyd.elec5619.service.UserService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	private CommitmentManager commitmentManager;
	
	@Autowired
	private UserService userService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value ="/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {

		    /* The user is logged in :) */
			return "redirect:/hello";
		}
		
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping(value="/hello", method = RequestMethod.GET)
	public String hello(Model model) {
		Account user = getAuthAccount();
		if(user == null) return "redirect:/j_spring_security_logout";
		logger.info("User " + user.getEmail());
		model.addAttribute("userEmail", user.getEmail());
		model.addAttribute("today", new Date().getTime());
		model.addAttribute("commitments", commitmentManager.getActiveCommitmentsByUser(user));
		model.addAttribute("following", user.getFollowing());
		logger.info(user.getFollowing().toString());
		logger.info(user.getFollowers().toString());
		return "hello";
	}
	
	public Account getAuthAccount() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		Account user = userService.getUserByEmail(email);
		return user;
	}
}
