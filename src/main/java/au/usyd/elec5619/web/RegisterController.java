package au.usyd.elec5619.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.exception.EmailExistsException;
import au.usyd.elec5619.service.IUserService;

@Controller
public class RegisterController {
	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);
	
	@Autowired
	private IUserService userService;
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegister(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {

		    /* The user is logged in :) */
			return "redirect:/hello";
		}
		Account user = new Account();
		model.addAttribute("user", user);

		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postRegister(@Valid @ModelAttribute("user") Account user, BindingResult result, WebRequest request,
			Errors errors) {

		if (result.hasErrors()) {
			return "register";
		} else {
			try {
				userService.register(user);
			} catch (EmailExistsException e) {
				result.reject("register.fail", "Email already exists");
				return "register";
			}
		}

		return "redirect:/";
	}
}