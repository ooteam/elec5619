package au.usyd.elec5619.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseCommitment;
import au.usyd.elec5619.domain.SmokingCommitment;
import au.usyd.elec5619.domain.WeightCommitment;
import au.usyd.elec5619.service.CommitmentForm;
import au.usyd.elec5619.service.CommitmentManager;
import au.usyd.elec5619.service.UserService;

@Controller
public class CommitmentController {

	@Autowired
	private CommitmentManager commitmentManager;

	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(CommitmentController.class);

	@RequestMapping(value="/exerciseCommitment")
	public String exerciseCommitment(Model model) {

		logger.info("Opening page to create new exercise commitment");

		model.addAttribute("commitmentForm", new CommitmentForm());

		return "exerciseCommitment";
	}

	@RequestMapping(value="/weightCommitment")
	public String weightCommitment(Model model) {

		logger.info("Opening page to create new exercise commitment");

		model.addAttribute("commitmentForm", new CommitmentForm());

		return "weightCommitment";
	}

	@RequestMapping(value="/smokingCommitment")
	public String smokingCommitment(Model model) {

		logger.info("Opening page to create new smoking commitment");

		model.addAttribute("commitmentForm", new CommitmentForm());

		return "smokingCommitment";
	}



	@RequestMapping(value="/postCommitment", method = RequestMethod.POST)
	public String postExerciseCommitment(HttpServletRequest request, HttpServletResponse response,
			CommitmentForm commitmentForm, BindingResult result) {
		if (result.hasErrors()) {
			return "redirect:/hello";
		} else {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Account user = userService.getUserByEmail(auth.getName());
			Commitment commitment = null;
			logger.info("Commitment type is " + commitmentForm.getType());
			if (commitmentForm.getType().equals("Exercise")) {
				logger.info("Initiating commitment as exercise");
				commitment =
						new ExerciseCommitment(user, commitmentForm.getEndDate(), commitmentForm.getWeeklyBet(),
								commitmentForm.getWeeklyGoal());
			} else if (commitmentForm.getType().equals("Weight")) {
				logger.info("Initiating commitment as weightloss");
				commitment = new WeightCommitment(user, commitmentForm.getEndDate(), commitmentForm.getWeeklyBet(),
						commitmentForm.getStartWeight(), commitmentForm.getWeeklyLossGoal());
			} else if (commitmentForm.getType().equals("Smoke")) {
				logger.info("Initiating commitment as smoking");
				commitment = new SmokingCommitment(user, commitmentForm.getEndDate(), commitmentForm.getWeeklyBet(),
						commitmentForm.getWeeklyGoal());
			}
			logger.info("Saving commitment with commitment manager " + commitment.toString());
			commitmentManager.createCommitment(commitment);
		}

		return "redirect:/hello";
	}

	@RequestMapping(value="/dailyCheck", method = RequestMethod.GET)
	public String dailyCheck(){
		commitmentManager.dailyPenaltyCheck();
		return "redirect:/hello";
	}



}
