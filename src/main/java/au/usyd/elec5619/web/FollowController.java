package au.usyd.elec5619.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.Progress;
import au.usyd.elec5619.domain.SmokingProgress;
import au.usyd.elec5619.domain.WeightProgress;
import au.usyd.elec5619.service.CommitmentManager;
import au.usyd.elec5619.service.ProgressService;
import au.usyd.elec5619.service.UserService;

@Controller
public class FollowController {
	
	@Autowired
	private CommitmentManager commitmentManager;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProgressService progService;
	
	private static final Logger logger = LoggerFactory.getLogger(FollowController.class);
	
	
	@RequestMapping(value="/listUsers", method = RequestMethod.GET)
	public String listUsers(Model model) {
		Account user = getAuthAccount();
		if(user == null) return "redirect:/j_spring_security_logout";
		
		List<Account> followableAccount = userService.getAllAccounts();
		followableAccount.remove(user);
		followableAccount.removeAll(user.getFollowing());
		
		model.addAttribute("users", followableAccount);
		return "listUsers";
	}
	
	@RequestMapping(value = "/follow/{id}", method = RequestMethod.GET)
	public String followUser(@PathVariable(value="id") Long id,
			Locale locale, Model model) {
		
		Account user = getAuthAccount();
		if(user == null) return "redirect:/j_spring_security_logout";
		Account following = userService.getUserById(id);
		Boolean success = userService.followUser(user, following);
		
		return "redirect:/hello";
		
	}
	
	@RequestMapping(value="/activities", method = RequestMethod.GET)
	public String activities(Model model) {
		Account user = getAuthAccount();
		if(user == null) return "redirect:/j_spring_security_logout";
		List<String> updates = new ArrayList<String>();
		for (Account a : user.getFollowing()) {
			logger.info("Activities for " + a.getEmail());
			Progress last = progService.getLastProgressByUser(a);
			if(last == null) continue;
			logger.info("Progress is " + last.getComment());
			if (last.getClass() == ExerciseProgress.class) {
				ExerciseProgress ex = (ExerciseProgress) last;
				String update = a.getEmail() + " did " + ex.getExerciseDone() + " km of " + ex.getExerciseType().toString() + " and weighed " + ex.getCurrentWeight() + " kg";
				updates.add(update);
				updates.add("Comment: " + ex.getComment());
			} else if (last.getClass() == SmokingProgress.class) {
				SmokingProgress smoke = (SmokingProgress) last;
				String update = a.getEmail() + " smoked " + smoke.getNumberOfCigarette() + " cigarettes";
				updates.add(update);
				updates.add("Comment: " + smoke.getComment());
			} else if (last.getClass() == WeightProgress.class) {
				WeightProgress weight = (WeightProgress) last;
				String update = a.getEmail() + " now weighs " + weight.getCurrentWeight() + " kg";
				updates.add(update);
				updates.add("Comment: " + weight.getComment());
			}
		}
		model.addAttribute("updates", updates);
		return "activities";
	}
	
	@RequestMapping(value="/followers", method = RequestMethod.GET)
	public String followers(Model model) {
		Account user = getAuthAccount();
		if(user == null) return "redirect:/j_spring_security_logout";
		
		model.addAttribute("followers", user.getFollowers());
		return "followers";
	}
	
	public Account getAuthAccount() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = auth.getName();
		Account user = userService.getUserByEmail(email);
		return user;
	}
		
}
