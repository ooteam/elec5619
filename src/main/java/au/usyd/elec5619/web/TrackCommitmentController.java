package au.usyd.elec5619.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import au.usyd.elec5619.domain.Account;
import au.usyd.elec5619.domain.Commitment;
import au.usyd.elec5619.domain.ExerciseProgress;
import au.usyd.elec5619.domain.ExerciseType;
import au.usyd.elec5619.domain.Progress;
import au.usyd.elec5619.domain.SmokingProgress;
import au.usyd.elec5619.domain.WeightProgress;
import au.usyd.elec5619.service.CommitmentManager;
import au.usyd.elec5619.service.ICommitmentManager;
import au.usyd.elec5619.service.IProgressService;
import au.usyd.elec5619.service.IUserService;
import au.usyd.elec5619.service.ProgressService;
import au.usyd.elec5619.service.UserService;

@Controller
@RequestMapping(value = "/track")
public class TrackCommitmentController {
	private static final Logger logger = LoggerFactory.getLogger(TrackCommitmentController.class);
	
	private static boolean dateEqual(Date a, Date b) {
		return a.getDate() == b.getDate() && a.getMonth() == b.getMonth() &&
				a.getYear() == b.getYear();
	}
	
	@Autowired
	IProgressService progressService = new ProgressService();
	
	@Autowired
	IUserService userService = new UserService();
	
	@Autowired
	ICommitmentManager commitmentService = new CommitmentManager();
	
	//ProgressValidator progressValidator;
	
	/*@Autowired
	public TrackCommitmentController(ProgressValidator progressValidator) {
		this.progressValidator = progressValidator;
	}*/

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String listTracking(Locale locale, Model model) {
		logger.info("Getting track");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		List<Commitment> commitments = commitmentService.getActiveCommitmentsByUser(user);
		
		model.addAttribute("commitments", commitments);
		return "track";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String showTracking(@PathVariable(value="id") String id,
			Locale locale, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		//List<Progress> progresses = commitmentService.getCommitmentByUser(user);
		Commitment commitment = commitmentService.getCommitmentById(new Long(id));
		Progress lastProgress = progressService.getLastProgressByCommitment(commitment);
		//List<Progress> progresses = progressService.getProgressesByCommitment(commitment); 
		
		Progress newProgress;
		if (commitment.getCommitmentTypeName() == "au.usyd.elec5619.domain.ExerciseCommitment") {
			newProgress = new ExerciseProgress(commitment);
		} else if (commitment.getCommitmentTypeName() == "au.usyd.elec5619.domain.SmokingCommitment") {
			newProgress = new SmokingProgress(commitment);
		} else {
			newProgress = new WeightProgress(commitment);
		}
		
		model.addAttribute("progress", newProgress);
		if (lastProgress == null) {
			model.addAttribute("reported", false);
		} else {
			model.addAttribute("reported", dateEqual(lastProgress.getProgressDate(), new Date()));
		}
		model.addAttribute("commitmentId", commitment.getId());
		model.addAttribute("commitment", commitment);
		model.addAttribute("commitmentType", commitment.getCommitmentTypeName().split("\\.")[4]);

		if (commitment.getCommitmentTypeName() == "au.usyd.elec5619.domain.ExerciseCommitment") {
			return "track_show_exercise";
		} else if (commitment.getCommitmentTypeName() == "au.usyd.elec5619.domain.SmokingCommitment") {
			return "track_show_smoking";
		} else {
			return "track_show_weight";
		}
	}
	
	@RequestMapping(value = "/create-exercise", method = RequestMethod.POST)
	public String processExerciseSubmit(
		@RequestParam(value="commitment", required=true) String commitmentId,
		@ModelAttribute("progress") @Valid ExerciseProgress progress,
		BindingResult result, WebRequest request,
		Errors errors) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		
		logger.info(progress.getExerciseType().toString());
		progress.setCommitment((commitmentService.getCommitmentById(Long.parseLong(commitmentId))));
		progress.setProgressDate(new Date());
		progress.setUser(user);
		
		//progressValidator.validate(progress, result);
		progressService.report(progress);
		
		return "redirect:/track/show/" + commitmentId;
	}
	
	@RequestMapping(value = "/create-smoking", method = RequestMethod.POST)
	public String processSmokingSubmit(
		@RequestParam(value="commitment", required=true) String commitmentId,
		@ModelAttribute("progress") @Valid SmokingProgress progress,
		BindingResult result, WebRequest request,
		Errors errors) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		
		progress.setCommitment((commitmentService.getCommitmentById(Long.parseLong(commitmentId))));
		progress.setProgressDate(new Date());
		progress.setUser(user);
		//progressValidator.validate(progress, result);
		progressService.report(progress);
		
		return "redirect:/track/show/" + commitmentId;
	}
	
	@RequestMapping(value = "/create-weight", method = RequestMethod.POST)
	public String processSmokingSubmit(
		@RequestParam(value="commitment", required=true) String commitmentId,
		@ModelAttribute("progress") @Valid WeightProgress progress,
		BindingResult result, WebRequest request,
		Errors errors) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Account user = userService.getUserByEmail(auth.getName());
		
		progress.setCommitment((commitmentService.getCommitmentById(Long.parseLong(commitmentId))));
		progress.setProgressDate(new Date());
		progress.setUser(user);
		//progressValidator.validate(progress, result);
		progressService.report(progress);
		
		return "redirect:/track/show/" + commitmentId;
	}
	
	
	@ModelAttribute("exerciseTypeList")
	public Map<String, String> populateExerciseTypeList() {
		Map<String, String> exerciseTypeList = new HashMap<String, String>();
		exerciseTypeList.put(ExerciseType.JOGGING.toString(), ExerciseType.JOGGING.name());
		exerciseTypeList.put(ExerciseType.RUNNING.toString(), ExerciseType.RUNNING.name());
		exerciseTypeList.put(ExerciseType.SWIMMING.toString(), ExerciseType.SWIMMING.name());
		return exerciseTypeList;
	}
}
