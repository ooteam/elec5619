package au.usyd.elec5619.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Progress")
@Inheritance(strategy=InheritanceType.JOINED) 
public abstract class Progress {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(targetEntity=Commitment.class)
	@JoinColumn(name="commitmentId")
	private Commitment commitment;
	
	@ManyToOne(targetEntity=Account.class)
	@JoinColumn(name="userId")
	private Account user;
	
	@Column(name = "progress_date")
	private Date progressDate;
	
	@Column(name = "comment")
	private String comment;
	
	public Progress(Commitment commitment) {
		this.commitment = commitment;
	}
	
	public Progress() {
	}
	
	public Commitment getCommitment() {
		return commitment;
	}
	public void setCommitment(Commitment commitment) {
		this.commitment = commitment;
	}
	
	public Date getProgressDate() {
		return progressDate;
	}
	public void setProgressDate(Date progressDate) {
		this.progressDate = progressDate;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public Account getUser() {
		return user;
	}
	public void setUser(Account user) {
		this.user = user;
	}
}
