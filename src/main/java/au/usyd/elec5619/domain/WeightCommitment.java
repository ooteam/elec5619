package au.usyd.elec5619.domain;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class WeightCommitment extends Commitment {

	private Double startWeight;
	private Double weeklyLossGoal; //percentage lost weekly
	private Double latestWeight;

	public WeightCommitment(){

	}
	
	public WeightCommitment(Account user, Date endDate, Integer weeklyBet, Double startWeight, Double weeklyLossGoal) {
		super(user, endDate, weeklyBet, "Nutrition Foundation", "Weight");
		this.startWeight = startWeight;
		this.weeklyLossGoal = weeklyLossGoal;
		this.latestWeight = startWeight;
	}

	public Double getStartWeight() {
		return startWeight;
	}

	public void setStartWeight(Double startWeight) {
		this.startWeight = startWeight;
	}

	public Double getWeeklyLossGoal() {
		return weeklyLossGoal;
	}

	public void setWeeklyLossGoal(Double weeklyLossGoal) {
		this.weeklyLossGoal = weeklyLossGoal;
	}
	
	public Double getLatestWeight() {
		return latestWeight;
	}

	public void setLatestWeight(Double latestWeight) {
		this.latestWeight = latestWeight;
	}

}
