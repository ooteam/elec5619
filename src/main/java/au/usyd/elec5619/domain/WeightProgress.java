package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "WeightProgress")
public class WeightProgress extends Progress {
	private double currentWeight;
	
	public WeightProgress(Commitment commitment) {
		super(commitment);
	}
	
	public WeightProgress() {
	}
	
	public WeightProgress(Commitment commitment, Double currentWeight) {
		super(commitment);
		this.currentWeight = currentWeight;
	}
	
	public double getCurrentWeight() {
		return currentWeight;
	}
	public void setCurrentWeight(double currentWeight) {
		this.currentWeight = currentWeight;
	}
	
}
