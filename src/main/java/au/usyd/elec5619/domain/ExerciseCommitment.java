package au.usyd.elec5619.domain;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class ExerciseCommitment extends Commitment {

	private Integer weeklyGoal; //number of times they commit to exercise in a week
	
	public ExerciseCommitment(){

	}

	public ExerciseCommitment(Account user, Date endDate, Integer weeklyBet, Integer weeklyGoal) {
		super(user, endDate, weeklyBet, "Heart Foundation", "Exercise");
		this.weeklyGoal = weeklyGoal;
	}
	
	public int getWeeklyGoal() {
		return weeklyGoal;
	}

	public void setWeeklyGoal(int weeklyGoal) {
		this.weeklyGoal = weeklyGoal;
	}
	
}
