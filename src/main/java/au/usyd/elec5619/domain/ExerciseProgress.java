package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ExerciseProgress")
public class ExerciseProgress extends Progress {
	private double currentWeight;
	private ExerciseType exerciseType;
	private double exerciseDone;
	
	public ExerciseProgress(Commitment commitment) {
		super(commitment);
	}
	
	public ExerciseProgress() {
	}
	
	public ExerciseProgress(Commitment commitment, Double currentWeight, 
			Double exerciseDone, ExerciseType exerciseType) {
		super(commitment);
		this.currentWeight = currentWeight;
		this.exerciseDone = exerciseDone;
		this.exerciseType = exerciseType;
	}
	
	public double getCurrentWeight() {
		return currentWeight;
	}
	public void setCurrentWeight(double currentWeight) {
		this.currentWeight = currentWeight;
	}
	
	public ExerciseType getExerciseType() {
		return exerciseType;
	}
	public void setExerciseType(ExerciseType exerciseType) {
		this.exerciseType = exerciseType;
	}
	
	public void setExerciseDone(double exerciseDone) {
		this.exerciseDone = exerciseDone;
	}
	public double getExerciseDone() {
		return exerciseDone;
	}
}
