package au.usyd.elec5619.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Commitment implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long commitmentId;
	
	@ManyToOne(targetEntity=Account.class)
	@JoinColumn(name="userId")
	private Account user;
	
	private Date startDate;
	private Date endDate;
	private Integer weeklyBet;
	private String charity;
	private String type; //Exercise, Weight, Smoke
	private Integer totalFail;

	public Commitment(){
		
	}
	
	public Commitment(Account user, Date endDate, Integer weeklyBet, String charity, String type) {
		this.user = user;
		this.endDate = endDate;
		this.weeklyBet = weeklyBet;
		this.charity = charity;
		this.type = type;
		this.startDate = new Date();
		this.totalFail = 0;
	}
	
	public Account getUserId() {
		return user;
	}
	public void setUser(Account user) {
		this.user = user;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getWeeklyBet() {
		return weeklyBet;
	}
	public void setWeeklyBet(Integer weeklyBet) {
		this.weeklyBet = weeklyBet;
	}
	public String getCharity() {
		return charity;
	}
	public void setCharity(String charity) {
		this.charity = charity;
	}
	public Long getId() {
		return commitmentId;
	}
		
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCommitmentTypeName() {
		return this.getClass().getName();
	}
	
	public Integer getTotalFail() {
		return totalFail;
	}

	public void setTotalFail(Integer totalFail) {
		this.totalFail = totalFail;
	}


}
