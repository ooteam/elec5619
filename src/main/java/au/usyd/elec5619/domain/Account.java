package au.usyd.elec5619.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.Email;
 
@Entity
@Table(name = "Account")
public class Account {
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Email(message="Please provide a valid email address")
	@Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
	@Column(name = "email", unique = true, nullable = false) 
	private String email;

	@Column(name = "password", nullable = false, length = 60)
	private String password;
	
	@Column(name = "enabled", nullable = false)
	private boolean enabled;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>(0);
	
    @ManyToMany(fetch = FetchType.EAGER)
    @Cascade({CascadeType.SAVE_UPDATE})
    @JoinTable(name="following",
        joinColumns={@JoinColumn(name="followerId")},
        inverseJoinColumns={@JoinColumn(name="followingId")})
    private Set<Account> following = new HashSet<Account>();
    
    @ManyToMany(fetch = FetchType.EAGER, mappedBy="following")
    private Set<Account> followers = new HashSet<Account>();

	public Account() {
	}
 
	public Account(String email, String password, boolean enabled) {
		this.email = email;
		this.password = password;
		this.enabled = enabled;
	}
	
	public Account(String email, String password, boolean enabled, Set<UserRole> userRole) {
		this.email = email;
		this.password = password;
		this.enabled = enabled;
		this.userRole = userRole;
	}
 
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getId() {
		return id;
	}
 

	public String getEmail() {
		return email;
	}
 
	public void setEmail(String email) {
		this.email = email;
	}
	

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}
	
	public Set<Account> getFollowing() {
		return following;
	}

	public void setFollowing(Set<Account> following) {
		this.following = following;
	}
	
	public void addFollowing(Account user) {
		this.following.add(user);
	}
	
	public Set<Account> getFollowers() {
		return followers;
	}

	public void setFollowers(Set<Account> followers) {
		this.followers = followers;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null)
	        return false;
	    if (!(obj instanceof Account))
	        return false;
	    Account other = (Account) obj;
	    return id == null ? false : this.id.equals(other.id);//Compare Id if null falseF
	}
	
	   @Override
	    public int hashCode() {
	        return new HashCodeBuilder(11, 31).
	            append(id).
	            append(email).
	            toHashCode();
	    }
}