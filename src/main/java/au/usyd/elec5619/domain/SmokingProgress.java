package au.usyd.elec5619.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "SmokingProgress")
public class SmokingProgress extends Progress {
	private int  numberOfCigarette;
	
	public SmokingProgress(Commitment commitment) {
		super(commitment);
	}
	
	public SmokingProgress() {
	}
	
	public SmokingProgress(Commitment commitment, int numberOfCigarette) {
		super(commitment);
		this.numberOfCigarette = numberOfCigarette;
	}
	
	public int getNumberOfCigarette() {
		return numberOfCigarette;
	}
	
	public void setNumberOfCigarette(int numberOfCigarette) {
		this.numberOfCigarette = numberOfCigarette;
	}
}
