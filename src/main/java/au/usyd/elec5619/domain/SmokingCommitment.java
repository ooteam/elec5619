package au.usyd.elec5619.domain;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class SmokingCommitment extends Commitment {

	private Integer weeklyGoal; //Maximum cigarette per week

	public SmokingCommitment(){}

	public SmokingCommitment(Account user, Date endDate, Integer weeklyBet, Integer weeklyGoal) {
		super(user, endDate, weeklyBet, "Cancer Council", "Smoke");
		this.weeklyGoal = weeklyGoal;
	}

	public Integer getWeeklyGoal() {
		return weeklyGoal;
	}

	public void setWeeklyGoal(Integer weeklyGoal) {
		this.weeklyGoal = weeklyGoal;
	}

}
