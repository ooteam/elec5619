package au.usyd.elec5619.domain;

public enum ExerciseType {
	RUNNING("Running"), 
	SWIMMING("Swimming"), 
	JOGGING("Jogging");
	
	private String name;

    private ExerciseType(String name) {
        this.name = name;
    }
	
	public String toString() {
		return this.name.substring(0, 1).toUpperCase() + this.name.substring(1).toLowerCase();
	}
}
