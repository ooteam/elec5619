#Setting up PostgreSQL
In the pom.xml the setting is for Postgres version: 9.4-1200-jdbc41  
So try to have PostgreSQL 9.4 and Java version at least 1.7  

Database Name = carrotandstick  
Schema = public  
Username = carrot  
Passsword = stick  

```sql
CREATE DATABASE carrotandstick;
CREATE USER carrot WITH SUPERUSER PASSWORD 'stick';
```


#Description
Mention health problems in Australia.

  - Obesity: http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/by%20Subject/4338.0~2011-13~Main%20Features~Overweight%20and%20obesity~10007

  - Mention how exercise can help.

  - Smoking: http://www.abs.gov.au/ausstats/abs@.nsf/Lookup/4125.0main+features3320Jan%202013

Mention research shows that sharing their progress with others and putting money on the line increases the successful rate of achieving a goal.

  - Putting bet as incentive: Carrots and Sticks: Unlock the Power of Incentives to Get Things Done

  - Sharing experience: http://www.dominican.edu/dominicannews/study-highlights-strategies-for-achieving-goals

The idea of the project is to provide a platform that allow people to share their progress with their friends and using charitable giving as incentives.

#Components:
  - Making a commitment
  - Tracking a commitment
  - Friendship

#Other components:
  - Home page
  - Registration
  - etc.

#Making a commitment:
  - User selects the type of commitment she wants to make (either losing weight or quit smoking)
  - User decides how long the commitment should last
    - May be also set a minimum for quit smoking (e.g. at least 10 weeks)
  - User can make a schedule?
    - For example, running 30km in total in the first week, 50km in the second week.
    - Place a bet on each component.
  - User selects how much she wants to bet on the promise.
  - The amount she bets goes to charity (not selectable, it will be organisation that help people to quit smoking if the commitment type is quit smoking, vice versa).
  - Create a Commitment table. Store all the information.
  - Provide an API for other components to get commitment info.
  - Simulates Bank transfer (probably just create a Bank model)
  - Provide an API to mark the commitment as unsuccessful and charge user in her Bank account.
  - May need a charity table to simulate charity entity. The charity can be an owner of a Bank account.

# Integrating with Fitbit (THIS IDEA IS NOW REMOVED)
  - Load data from Fitbit

#Tracking a commitment:
  - User can report on the progress of achieving their goal on a daily basis.
  - Reporting on daily basis is optional. It helps the system to create visualisation.
  - Reporting on weekly basis is mandatory. Failing to do so means breaking the promise and the money will be donated to the charity.
  - User enters how much she has done in a day or week (e.g. run 8km today or this week).
  - User can make comments on the progress.
  - Visualisation:
    - Line graph/heatmap for weight loss, etc.

#Friendship:
  - View user's profile and modify user's profile.
  - Users can be friends of each other. This includes making friend request and accepting friend request.
  - The progress of achieving a promise will be shown in user's profile. The progress of their friends can be obtained by calling tracking commitment API. Allow commenting on the progress?
  - Leaderboard
    - Friends who made the same type of promise can see who is fulfilling their promises.
    - Visualisation and ranking based on the statistics.