\contentsline {section}{\numberline {1}Project Detail}{2}{section.1}
\contentsline {section}{\numberline {2}Purpose of this document}{2}{section.2}
\contentsline {section}{\numberline {3}Project Overview}{2}{section.3}
\contentsline {section}{\numberline {4}Research and Background}{2}{section.4}
\contentsline {section}{\numberline {5}Concept}{3}{section.5}
\contentsline {section}{\numberline {6}Task Allocation}{3}{section.6}
\contentsline {section}{\numberline {7}Design}{4}{section.7}
\contentsline {subsection}{\numberline {7.1}Overview}{4}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Model}{4}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Service}{5}{subsection.7.3}
\contentsline {section}{\numberline {8}Components}{6}{section.8}
\contentsline {subsection}{\numberline {8.1}Progress component}{6}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Model}{6}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Service}{7}{subsubsection.8.1.2}
\contentsline {section}{\numberline {9}Mockups}{7}{section.9}
